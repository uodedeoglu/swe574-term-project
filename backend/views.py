from django.http import HttpResponse
from django.http.response import HttpResponseNotAllowed
from backend.fetchArticles import fetchArticles

def void(request):
    if request.user.is_superuser:
        return HttpResponse("Not implemented yet")
    else:
        return HttpResponse("Insufficient permissions to view this page.")

def fetch(request):
    if request.user.is_superuser:
        fetchArticles()
        return HttpResponse("Articles fetched.")
    else:
        return HttpResponse("Insufficient permissions to view this page.")
