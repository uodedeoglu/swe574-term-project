import requests
import re
from backend.populateDatabase import addArticle, cleanUp
from django.db import connection
from django.db.utils import IntegrityError

def fetchArticles():
    database = 'pubmed'
    query = 'depression'

    base = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/'
    url = base + "esearch.fcgi?db=" + database + "&term=" + query + "&usehistory=y"

    request = requests.get(url)
    data = request.text

    web = re.search("(?<=<WebEnv>)(.*?)(?=</WebEnv>)", data).group()
    key = re.search("(?<=<QueryKey>)(.*?)(?=</QueryKey>)", data).group()

    # Article fetch type
    rettype = "medline"
    retmode = "text"

    count = 50000 # Total article number to pull
    retmax = 5000 # Batches to pull articles in

    for retstart in range(0, count, retmax):
        # Finish the URL for the request
        efetch_url = base + "efetch.fcgi?db=" + database + "&WebEnv=" + web
        efetch_url += "&query_key=" + key + "&retstart=" + str(retstart)
        efetch_url += "&retmax=" + str(retmax) + "&rettype=" + rettype 
        efetch_url += "&retmode=" + retmode
        efetch_out = requests.get(efetch_url)

        # Split the articles
        split_text = efetch_out.text.split("PMID- ")

        # Add articles to the database 1 by 1
        for article in split_text:
            if (article != '\n'):
                addArticle(article)

        print("Loop " + str(int(retstart / retmax) + 1) + " of " + str(int(count/retmax)) + " finished.")

    print("All articles imported!\n")

    cleanUp()
    populateSearchVector()

    print("Clean up finished!")


def populateSearchVector():
    try:
        cursor = connection.cursor()
        cursor.execute("""select "search_vector_update"();;""")
    except Exception as e:
        print(e)
        cursor.close

