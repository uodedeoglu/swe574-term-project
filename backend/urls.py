from django.urls import path
from django.contrib import admin
from . import views

urlpatterns = [
    path('fetch', views.fetch, name='fetch'),
    path('adminpanel', admin.site.urls, name="adminpanel"),
    path('void', views.void, name='void')
]
