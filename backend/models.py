from django.db import models
import uuid
from usermanagement.models import User
from django.db import models
from django.contrib.postgres.search import SearchVectorField
from django.contrib.postgres.indexes import GinIndex

class Tag(models.Model):
    unique_id = models.UUIDField(default=uuid.uuid4, editable=False, unique=False)
    # wikidate_label
    label = models.CharField(max_length=1024)
    # wikidata_id
    wiki_q = models.CharField(max_length=1024)
    
    wiki_url = models.CharField(max_length=1024)
    # wikidata_description
    wiki_description = models.CharField(max_length=1024)
    # wikidata_alias
    custom_description = models.CharField(max_length=1024)
    # fetch related wiki objects from wikidata
    related_wiki_objects = models.CharField(max_length=15000)
    # fetch related wiki objects from WEMBEDDER Embedding Similarity
    similar_wiki_objects = models.CharField(max_length=15000)


class Highlight(models.Model):
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE, null=True)
    sub_text = models.CharField(max_length=15000, default="")


class Author(models.Model):
    name = models.CharField(max_length=1024)
    
    def __str__(self):
        return self.name


class Activity(models.Model):
    JSON_Field = models.JSONField()


class Article(models.Model):
    pmid = models.CharField(max_length=16, unique=True)
    title = models.CharField(max_length=1024)
    authors = models.ManyToManyField(Author)
    keywords = models.CharField(max_length=15000)
    tags = models.ManyToManyField(Tag)
    highlights = models.ManyToManyField(Highlight) 
    abstract = models.TextField(max_length=15000)
    publish_date = models.DateTimeField(null=True, blank=True)
    search_vector = SearchVectorField(null=True)

    class Meta:
        ordering = ['pmid']
        indexes = [GinIndex(fields=['search_vector'])]
        

    def __str__(self):
        return self.pmid


class ReportArticle(models.Model):
    pmid = models.CharField(max_length=15, null=True)
    user_report_id = models.ForeignKey(User, on_delete=models.CASCADE)
    report_message = models.TextField(max_length=300)
    report_date = models.DateTimeField(auto_now_add=True)
    is_open = models.BooleanField(default=True)

    def __str__(self):
        return 'Reported Article PMID: ' + self.pmid
