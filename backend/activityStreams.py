from backend.models import *
from datetime import datetime
import json

# Saves the activity of a tag being added to an article to the database / Type = Add
# Users can get notified about followed tags being added to a new article 
# or followed articles getting a new tag
def tag_added(request, tag, pmid):
    user_email = str(request.user.email)
    username = str(request.user.name)
    current_time = datetime.now()

    label = tag.label
    tag_id = str(tag.unique_id)
    wiki_q = tag.wiki_q
    wiki_url = tag.wiki_url
    wiki_description = tag.wiki_description
    custom_description = tag.custom_description

    article_pmid = pmid

    if label != '':
        tag_name = label
    else:
        tag_name = custom_description

    new_activity = {
        "@context": "https://www.w3.org/ns/activitystream",
        "summary": username + " added a Tag(" + tag_name + ") to the Article with PMID: " + article_pmid,
        "type": "Add",
        "published": current_time.strftime("%d/%m/%YT%H:%M:%S"),
        "actor": {
            "type": "Person",
            "id": user_email
        },
        "object": {
            "type": "Tag",
            "id": tag_id,
            "name": tag_name,
            "url": wiki_url,
            "content": wiki_q + "+" + wiki_description + "+" + custom_description,
            "attributedTo":{
                "type": "Article",
                "id": article_pmid
            }
        }
    }

    Activity.objects.create(JSON_Field=new_activity)
    return

def annotation_added(request, annotation, pmid):
    user_email = str(request.user.email)
    username = str(request.user.name)
    current_time = datetime.now()

    label = annotation.tag.label
    tag_id = str(annotation.tag.unique_id)
    wiki_q = annotation.tag.wiki_q
    wiki_url = annotation.tag.wiki_url
    wiki_description = annotation.tag.wiki_description
    custom_description = annotation.tag.custom_description
    
    sub_text = annotation.sub_text

    article_pmid = pmid

    if label != '':
        tag_name = label
    else:
        tag_name = custom_description

    new_activity = {
        "@context": "https://www.w3.org/ns/activitystream",
        "summary": username + " added a Annotation with (" + tag_name + ") to the Article with PMID: " + article_pmid + " with sub-text:" + sub_text,
        "type": "Add",
        "published": current_time.strftime("%d/%m/%YT%H:%M:%S"),
        "actor": {
            "type": "Person",
            "id": user_email
        },
        "object": {
            "type": "Tag",
            "id": tag_id,
            "name": tag_name,
            "url": wiki_url,
            "content": wiki_q + "+" + wiki_description + "+" + custom_description,
            "attributedTo":{
                "type": "Article",
                "id": article_pmid
            }
        }
    }

    Activity.objects.create(JSON_Field=new_activity)
    return

# Saves the activity of a user following another user/article/tag to the database / Type = Follow
# Users can get notified about followed users following other users/articles/tags
def user_follow(user_email, object_id, object_type, summary_user, summary_object):
    current_time = datetime.now()

    new_activity = {
        "@context": "https://www.w3.org/ns/activitystream",
        "summary": summary_user + " is now following " + object_type + ":" + summary_object,
        "type": "Follow",
        "published": current_time.strftime("%d/%m/%YT%H:%M:%S"),
        "actor": {
            "type": "Person",
            "id": user_email
        },
        "object": {
            "type": object_type,
            "id": object_id
        }
    }

    Activity.objects.create(JSON_Field=new_activity)

    return


# Saves the activity of a user unfollowing another user/article/tag to the database / Type = Remove
# Users can get notified about followed users unfollowing other users/articles/tags
def user_unfollow(user_email, object_id, object_type, summary_user, summary_object):
    current_time = datetime.now()

    new_activity = {
        "@context": "https://www.w3.org/ns/activitystream",
        "summary": summary_user + " has unfollowed " + object_type + ":" + summary_object,
        "type": "Remove",
        "published": current_time.strftime("%d/%m/%YT%H:%M:%S"),
        "actor": {
            "type": "Person",
            "id": user_email
        },
        "object": {
            "type": object_type,
            "id": object_id
        }
    }

    Activity.objects.create(JSON_Field=new_activity)

    return


# Queries the database for activities using a parameter
def get_activity(request):
    followed_activities = []
    json_activities = []
    usable_json_activities = []

    for user in request.user.followed_users.all():
        followed_user_activies = Activity.objects.filter(JSON_Field__actor__id = user.email)
        for followed_user_activity in followed_user_activies:
            if followed_user_activity not in followed_activities:
                followed_activities.append(followed_user_activity)

    for tag in request.user.followed_tags.all():
        followed_tag_activies = Activity.objects.filter(JSON_Field__object__id = tag.unique_id)
        for followed_tag_activity in followed_tag_activies:
            if followed_tag_activity not in followed_activities:
                followed_activities.append(followed_tag_activity)

    for article in request.user.followed_articles.all():
        followed_article_activies = Activity.objects.filter(JSON_Field__object__attributedTo__id = article.pmid)
        for followed_article_activity in followed_article_activies:
            if followed_article_activity not in followed_activities:
                followed_activities.append(followed_article_activity)

    for activity in followed_activities:
        json_field = json.dumps(activity.JSON_Field)
        usable_json = json.loads(json_field)
        usable_json_activities.append(usable_json)

    newlist = sorted(usable_json_activities, key=lambda d: d['published']) 
    newlist.reverse()

    for activity in newlist:
        json_field = json.dumps(activity)
        json_activities.append(json_field)

    return json_activities[:20]


####################################
### BELOW FUNCTIONS NOT USED YET ###
####################################

#? Saves the activity of a tag being created to the database / Type = Create
#? Users can get notified about followed users creating new tags
def tag_created(request):
    username = str(request.user.id)
    current_time = datetime.now()

    label = "label"
    wiki_q = "wiki_q"
    wiki_url = "wiki_url"
    wiki_description = "wiki_description"
    custom_description = "custom_description"

    article_pmid = "article_pmid"

    new_activity = {
        "@context": "https://www.w3.org/ns/activitystream",
        "summary": username + " created a Tag(" + label + ") and added to the Article with PMID:" + article_pmid,
        "type": "Create",
        "published": current_time.strftime("%d/%m/%YT%H:%M:%S"),
        "actor": {
            "type": "Person",
            "name": username
        },
        "tag": {
            "type": "Object",
            "name": label,
            "url": wiki_url,
            "content": wiki_q + "+" + wiki_description + "+" + custom_description,
            "attributedTo":[{
                "type": "Article",
                "name": article_pmid
            }]
        }
    }

    Activity.objects.create(JSON_Field=new_activity)

    return

#? Saves the activity of an article being published to the database / Type = Create
#? Users can get notified about followed users/authors publishing new articles
def article_published(request):
    username = str(request.user.id)
    current_time = datetime.now()

    article_pmid = "article_pmid"
    
    new_activity = {
        "@context": "https://www.w3.org/ns/activitystream",
        "summary": username + " published an Article with the PMID:" + article_pmid,
        "type": "Create",
        "published": current_time.strftime("%d/%m/%YT%H:%M:%S"),
        "actor": {
            "type": "Person",
            "name": username
        }
    }

    Activity.objects.create(JSON_Field=new_activity)

    return

#? Saves the activity of a query being created to the database / Type = Create
#? Users can get notified about followed users creating new search queries
def query_created(request):
    username = str(request.user.id)
    current_time = datetime.now()
    
    search_query = "search_query"

    new_activity = {
        "@context": "https://www.w3.org/ns/activitystream",
        "summary": username + " created a new Search Query",
        "type": "Create",
        "published": current_time.strftime("%d/%m/%YT%H:%M:%S"),
        "actor": {
            "type": "Person",
            "name": username
        },
        "object": {
            "type": "Note",
            "name": "Search Query",
            "content": search_query
        }
    }

    Activity.objects.create(JSON_Field=new_activity)

    return

#? Saves the activity of a tag being deleted to the database / Type = Delete
#? Users can get notified about followed users deleting tags
#? or followed tags being deleted
def tag_deleted(request):
    username = str(request.user.id)
    current_time = datetime.now()

    label = "label"
    wiki_q = "wiki_q"
    wiki_url = "wiki_url"
    wiki_description = "wiki_description"
    custom_description = "custom_description"

    new_activity = {
        "@context": "https://www.w3.org/ns/activitystream",
        "summary": username + " deleted the Tag(" + label + ")",
        "type": "Delete",
        "published": current_time.strftime("%d/%m/%YT%H:%M:%S"),
        "actor": {
            "type": "Person",
            "name": username
        },
        "tag": {
            "type": "Object",
            "name": label,
            "url": wiki_url,
            "content": wiki_q + "+" + wiki_description + "+" + custom_description
        }
    }

    Activity.objects.create(JSON_Field=new_activity)

    return

# Saves the activity of a tag being removed from an article to the database / Type = Remove
# Users can get notified about followed tags being removed
# or tags being removed from followed articles
def tag_removed(request):
    username = str(request.user.id)
    current_time = datetime.now()

    label = "label"
    wiki_q = "wiki_q"
    wiki_url = "wiki_url"
    wiki_description = "wiki_description"
    custom_description = "custom_description"

    article_pmid = "article_pmid"

    new_activity = {
        "@context": "https://www.w3.org/ns/activitystream",
        "summary": username + " removed the Tag(" + label + ") from the Article with PMID: " + article_pmid,
        "type": "Remove",
        "published": current_time.strftime("%d/%m/%YT%H:%M:%S"),
        "actor": {
            "type": "Person",
            "name": username
        },
        "tag": {
            "type": "Object",
            "name": label,
            "url": wiki_url,
            "content": wiki_q + "+" + wiki_description + "+" + custom_description,
            "attributedTo":[{
                "type": "Article",
                "name": article_pmid
            }]
        }
    }

    Activity.objects.create(JSON_Field=new_activity)

    return

#? Saves the activity of an article being displayed by a user / Type = Display
#? Users can see statistics about an article
def article_displayed(request, article):
    username = str(request.user.id)
    current_time = datetime.now()

    article_pmid = article.pmid
    
    new_activity = {
        "@context": "https://www.w3.org/ns/activitystream",
        "summary": username + " displayed an Article with the PMID:" + article_pmid,
        "type": "Display",
        "published": current_time.strftime("%d/%m/%YT%H:%M:%S"),
        "actor": {
            "type": "Person",
            "name": username
        },
        "pmid" : article_pmid
    }

    Activity.objects.create(JSON_Field=new_activity)

    return

#? Saves the activity of keyword(s) being searched by a user / Type = Display
#? Users can see statistics about an article
def query_searched(request, query_term):
    username = str(request.user.id)
    current_time = datetime.now()
    
    new_activity = {
        "@context": "https://www.w3.org/ns/activitystream",
        "summary": username + " searched for articles with the query term of:" + query_term,
        "type": "Search",
        "published": current_time.strftime("%d/%m/%YT%H:%M:%S"),
        "actor": {
            "type": "Person",
            "name": username
        },
        "query_term": query_term
    }

    Activity.objects.create(JSON_Field=new_activity)

    return

#? Saves the activity of query being searched by a user / Type = Save
#? Users can see statistics about an article
def query_saved(request, query_term):
    username = str(request.user.id)
    current_time = datetime.now()
    
    new_activity = {
        "@context": "https://www.w3.org/ns/activitystream",
        "summary": username + " saved a search with the query term of:" + query_term,
        "type": "QuerySaved",
        "published": current_time.strftime("%d/%m/%YT%H:%M:%S"),
        "actor": {
            "type": "Person",
            "name": username
        },
        "query_term": query_term
    }

    Activity.objects.create(JSON_Field=new_activity)

    return

def query_deleted(request, query_term):
    username = str(request.user.id)
    current_time = datetime.now()
    
    new_activity = {
        "@context": "https://www.w3.org/ns/activitystream",
        "summary": username + " deleted a saved search with the query term of:" + query_term,
        "type": "QueryDeleted",
        "published": current_time.strftime("%d/%m/%YT%H:%M:%S"),
        "actor": {
            "type": "Person",
            "name": username
        },
        "query_term": query_term
    }
    
    try:    
        Activity.objects.filter(JSON_Field__type= "QuerySaved", JSON_Field__query_term = query_term, JSON_Field__actor__name = str(request.user.id)).delete()
        Activity.objects.create(JSON_Field=new_activity)
    except Exception as e:
        print(e)

    return       