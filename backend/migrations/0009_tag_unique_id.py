# Generated by Django 3.2.8 on 2021-12-26 17:34

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0008_article_backend_art_search__ea16b3_gin'),
    ]

    operations = [
        migrations.AddField(
            model_name='tag',
            name='unique_id',
            field=models.UUIDField(default=uuid.uuid4, editable=False),
        ),
    ]
