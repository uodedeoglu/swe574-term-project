from backend.models import Article, Author, Highlight
from dateutil import parser
from datetime import date
import warnings

def addArticle(article):
    warnings.filterwarnings("ignore")
    flag = ""
    pmid = ""
    publish_date = ""
    title = ""
    abstract = ""
    name = ""
    keywords = ""
    
    article = "PMID- " + article
    lines = article.split("\n")

    for line in lines:
        if "- " in line:
            identifier = line.split("- ", 1)[0].strip()
            body = line.split("- ", 1)[1].strip()
            flag = identifier

            if identifier == "PMID":
                pmid = body

                if not Article.objects.filter(pmid=pmid).exists():
                    article = Article.objects.create(pmid=pmid)
                    article.save()

            elif identifier == "DP":
                try:
                    publish_date = parser.parse(body)
                except:
                    publish_date = date.today()

                article = Article.objects.get(pmid=pmid)
                article.publish_date = publish_date
                article.save(update_fields=["publish_date"])

            elif identifier == "TI":
                title = body

                article = Article.objects.get(pmid=pmid)
                article.title = title
                article.save(update_fields=["title"])

            elif identifier == "AB":
                abstract = body

                article = Article.objects.get(pmid=pmid)
                article.abstract = abstract
                article.save(update_fields=["abstract"])

            elif identifier == "FAU":
                name = body

                article = Article.objects.get(pmid=pmid)
                exists = Author.objects.filter(name=name)
                if len(exists) > 0:
                    author_object = exists[0]
                else:
                    author_object = Author.objects.create(name=name)
                    author_object.save()
                article.authors.add(author_object)

            elif identifier == "OT":
                article = Article.objects.get(pmid=pmid)
                if (article.keywords != ""):
                    keywords = article.keywords + ", " + body
                else:
                    keywords = body
                article.keywords = keywords
                article.save(update_fields=["keywords"])

        else:
            body = line.strip()

            if flag == "TI":
                title += " " + body
                article = Article.objects.get(pmid=pmid)
                article.title = title
                article.save(update_fields=["title"])

            elif flag == "AB":
                abstract += " " + body
                article = Article.objects.get(pmid=pmid)
                article.abstract = abstract
                article.save(update_fields=["abstract"])

            elif flag == "FAU":
                author_object = Author.objects.get(name=name)
                name += " " + body
                author_object.name = name
                author_object.save(update_fields=["name"])
            
            elif flag == "OT":
                keywords += " " + body
                article = Article.objects.get(pmid=pmid)
                article.keywords = keywords
                article.save(update_fields=["keywords"])

def cleanUp():
    print(str(Article.objects.filter(title__exact='').count()) + " articles are found with no titles.")
    Article.objects.filter(title__exact='').delete()
    print("Removed the articles with no titles.")
    print(str(Article.objects.filter(title__exact='').count()) + " articles are found with no titles.\n")

    print(str(Author.objects.filter(article__isnull=True).count()) + " authors are found with no articles.")
    Author.objects.filter(article__isnull=True).delete()
    print("Removed the authors with no articles.")
    print(str(Author.objects.filter(article__isnull=True).count()) + " authors are found with no articles.\n")
