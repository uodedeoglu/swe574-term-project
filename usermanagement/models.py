from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
import uuid


class MyUserManager(BaseUserManager):
    def create_user(self, email, name, bio, password=None):
        if not email:
            raise ValueError("Users must have email adress")
        if not name:
            raise ValueError("Users must have name")
        if not bio:
            raise ValueError("Users must fill bio")

        user = self.model(
            email=self.normalize_email(email),
            name=name,
            bio=bio,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, name, bio, password):
        user = self.create_user(email=self.normalize_email(
            email), name=name, bio=bio, password=password)
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.is_active = True
        user.save(using=self._db)
        return user


class FollowedTag(models.Model):
    unique_id = models.CharField(max_length=64, unique=True)


class FollowedArticle(models.Model):
    pmid = models.CharField(max_length=16, unique=True)


class User(AbstractBaseUser):
    email = models.EmailField(verbose_name="Email", max_length=60, unique=True)
    name = models.CharField(verbose_name="Name", max_length=30)
    bio = models.CharField(verbose_name="Bio", max_length=300)
    date_joined = models.DateTimeField(
        verbose_name="Date Joined", auto_now_add=True)
    last_login = models.DateTimeField(verbose_name="Last Login", auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_approved = models.BooleanField(default=False)

    followed_users = models.ManyToManyField(
        'self', symmetrical=False, blank=True)
    followed_articles = models.ManyToManyField(FollowedArticle, blank=True)
    followed_tags = models.ManyToManyField(FollowedTag, blank=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name', 'bio']

    objects = MyUserManager()

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True

    def has_approval(self):
        return self.is_approved
