from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from usermanagement.models import User
from django.db import IntegrityError
from .forms import signupForm, loginForm
from django.http import HttpResponse
from django.contrib import messages

def userSignup(request):
    if request.method == "GET":
        if request.user.is_authenticated:
            return redirect('index')
        else:
            # UserCreationForm()})
            return render(request, "signup.html", {'form': signupForm()})
    else:
        if(request.POST['password1'] == request.POST['password2']):
            try:
                user = User.objects.create_user(
                    request.POST['email'], request.POST['name'], request.POST['bio'], password=request.POST['password1'])
                user.save()
                login(request, user)
                messages.success(
                    request, 'Sign up request received by MediPub Management. It will be processed shortly.')
                return redirect('index')
            except IntegrityError:
                return render(request, "signup.html", {'form': signupForm(), 'error': 'That email has been already taken. Please choose another email.'})
            '''except:
                return render(request, "signup.html", {'form': signupForm(), 'error': 'User can not be created.'})'''
        else:
            return render(request, "signup.html", {'form': signupForm(), 'error': 'Passwords did not match! Please check passwords.'})


def userLogin(request):
    if request.method == "GET":
        if request.user.is_authenticated:
            return redirect('index')
        else:
            # AuthenticationForm()})
            return render(request, 'login.html', {'form': loginForm()})
    else:
        user = authenticate(
            username=request.POST['email'], password=request.POST['password'])
        try:
            exist_user = User.objects.get(email=request.POST['email'])
            exist_user_flag = True
        except:
            exist_user_flag = False
        if user is not None:
            login(request, user)
            return redirect('index')
        elif exist_user_flag == False:
            return render(request, 'login.html', {'form': loginForm(), 'error': 'Username is not exist.'})

        elif (User.objects.get(email=request.POST['email']) is not None):
            current_user = User.objects.get(email=request.POST['email'])
            user_active_status_flag = current_user.is_active
            if user_active_status_flag == False:
                messages.success(request, 'Submission received. Acceptance Status: Pending')
                messages.success(request, 'Please medipub574@gmail.com, if any inconvenience')
                return redirect('index')
            else:
                return render(request, 'login.html', {'form': loginForm(), 'error': 'Username or Password is wrong. Please try again.'})


        else:
            return render(request, 'login.html', {'form': loginForm(), 'error': 'Username or Password is wrong. Please try again.'})


def userLogout(request):
    if request.method == "POST":
        logout(request)
        return redirect('index')

def void(request):
    return HttpResponse("Not implemented yet")
