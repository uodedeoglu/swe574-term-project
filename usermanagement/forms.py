from django import forms

class signupForm(forms.Form):
    #Fields

    name = forms.CharField(
        label="Your name", min_length=6, max_length=20, required=True)
    email = forms.EmailField(
        label="Email", max_length=60, required=True)
    password1 = forms.CharField(
        label="Password", min_length=6, widget=forms.PasswordInput(),  required=True)
    password2 = forms.CharField(
        label="Re-enter password", widget=forms.PasswordInput(), required=True)
    bio = forms.CharField(widget=forms.Textarea, max_length=300, required=True)

    # Widgets
    name.widget.attrs.update(
        {'class': 'form-control'})
    email.widget.attrs.update(
        {'class': 'form-control'})
    password1.widget.attrs.update(
        {'class': 'form-control', 'placeholder': 'at least 6 character'})
    password2.widget.attrs.update({'class': 'form-control'})
    bio.widget.attrs.update(
        {'class': 'form-control', 'placeholder': 'Please intoduce yourself inclduing your educational background, proffesion and workplace'})


class loginForm(forms.Form):
    email = forms.EmailField(
        label="Email address", max_length=100, required=True)
    password = forms.CharField(widget=forms.PasswordInput(),  required=True)
    email.widget.attrs.update(
        {'class': 'form-control'})
    password.widget.attrs.update(
        {'class': 'form-control'})
