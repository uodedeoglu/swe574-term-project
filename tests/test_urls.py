from django.shortcuts import resolve_url
from django.test import SimpleTestCase
from django.urls import reverse, resolve
from usermanagement.views import userSignup, userLogin, userLogout
from frontend.views import index, basic_search_result, show_article_detail, tag_article, show_report_page, submit_report_page, advanced_search, navigate_advanced_search, show_custom_tag_page, submit_custom_tag, search_tag, show_profile, show_feed, show_user_lookup, show_tag_lookup, follow_article, unfollow_article, follow_tag, unfollow_tag, follow_user, unfollow_user, search_user_lookup, search_tag_lookup, check_if_article_followed, return_followed_tags, return_followed_users
from backend.views import void,fetch

class TestUrls (SimpleTestCase):

    def test_signup_url(self):
        url = reverse('signup')
        print(resolve(url))
        self.assertEquals(resolve(url).func, userSignup)
    
    def test_login_url(self):
        url = reverse('login')
        print(resolve(url))
        self.assertEquals(resolve(url).func, userLogin)

    def test_logout_url(self):
        url = reverse('logout')
        print(resolve(url))
        self.assertEquals(resolve(url).func, userLogout)

    def test_index_url(self):
        url = reverse('index')
        print(resolve(url))
        self.assertEquals(resolve(url).func, index)

    def test_basic_search_result_url(self):
        url = reverse('results')
        print(resolve(url))
        self.assertEquals(resolve(url).func, basic_search_result)

#These tests fail because there are no reverse for this pages. Will be looked up later.

#def test_show_article_detail_url(self):
#    url = reverse('details')
#    print(resolve(url))
#    self.assertEquals(resolve(url).func, show_article_detail)

#def test_show_report_page_url(self):
#    url = reverse('show_report')
#    print(resolve(url))
#    self.assertEquals(resolve(url).func, show_report_page)

#def test_submit_report_page_url(self):
#    url = reverse('submit_report')
#    print(resolve(url))
#    self.assertEquals(resolve(url).func, submit_report_page)

    def test_advanced_url(self):
        url = reverse('advanced')
        print(resolve(url))
        self.assertEquals(resolve(url).func,navigate_advanced_search )

    def test_advanced_search_url(self):
        url = reverse('advancedSearch')
        print(resolve(url))
        self.assertEquals(resolve(url).func, advanced_search)

    #def test_show_custom_tag_url(self):
    #    url = reverse('show_custom_tag')
    #    print(resolve(url))
    #    self.assertEquals(resolve(url).func, show_custom_tag_page)

    def test_submit_custom_url(self):
        url = reverse('submit_custom')
        print(resolve(url))
        self.assertEquals(resolve(url).func, submit_custom_tag)

    def test_search_tag_url(self):
        url = reverse('search_tag')
        print(resolve(url))
        self.assertEquals(resolve(url).func, search_tag)

    def test_tag_article_url(self):
        url = reverse('tag_article')
        print(resolve(url))
        self.assertEquals(resolve(url).func, tag_article)

    def test_profile_url(self):
        url = reverse('profile')
        print(resolve(url))
        self.assertEquals(resolve(url).func,show_profile )

    #def test_follow_article_url(self):
    #    url = reverse('follow_article')
    #    print(resolve(url))
    #    self.assertEquals(resolve(url).func, follow_article)

    #def test_unfollow_article_url(self):
    #    url = reverse('unfollow_article')
    #    print(resolve(url))
    #    self.assertEquals(resolve(url).func, unfollow_article)

    #def test_follow_user_url(self):
    #    url = reverse('follow_user')
    #    print(resolve(url))
    #    self.assertEquals(resolve(url).func, follow_user)

    #def test_unfollow_user_url(self):
    #    url = reverse('unfollow_user')
    #    print(resolve(url))
    #    self.assertEquals(resolve(url).func, unfollow_user)

    #def test_follow_tag_url(self):
    #    url = reverse('follow_tag')
    #    print(resolve(url))
    #    self.assertEquals(resolve(url).func, follow_tag)

    #def test_unfollow_tag_url(self):
    #    url = reverse('unfollow_tag')
    #    print(resolve(url))
    #    self.assertEquals(resolve(url).func, unfollow_tag)

    def test_feed_url(self):
        url = reverse('feed')
        print(resolve(url))
        self.assertEquals(resolve(url).func, show_feed)

    def test_user_lookup_url(self):
        url = reverse('user_lookup')
        print(resolve(url))
        self.assertEquals(resolve(url).func, show_user_lookup)

    def test_search_user_url(self):
        url = reverse('search_user')
        print(resolve(url))
        self.assertEquals(resolve(url).func, search_user_lookup)

    def test_tag_lookup_url(self):
        url = reverse('tag_lookup')
        print(resolve(url))
        self.assertEquals(resolve(url).func, show_tag_lookup)

    def test_search_tag_url(self):
        url = reverse('search_tag')
        print(resolve(url))
        self.assertEquals(resolve(url).func, search_tag_lookup)

    def test_void(self):
        url = reverse('void')
        print(resolve(url))
        self.assertEquals(resolve(url).func, void)

    def test_fetch(self):
        url = reverse('fetch')
        print(resolve(url))
        self.assertEquals(resolve(url).func, fetch)
 
