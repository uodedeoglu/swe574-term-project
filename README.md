Term Project for the SWE 574 Software Development as a Team, 2021-2022 Fall, Boğaziçi University/ Istanbul, Turkey

Project was developed by Ulas Dedeoglu, Omer Gencer, Alp Batuhan, Ugur Tosun, Onat Uner.

Details can be found on the [wiki page](https://gitlab.com/uodedeoglu/swe574-term-project/-/wikis/home)

Please visit [system manual](https://gitlab.com/uodedeoglu/swe574-term-project/-/wikis/11.-System-Manual) for installation guide.
