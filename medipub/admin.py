from django.contrib import admin
from backend.models import *
from usermanagement.models import User, FollowedTag, FollowedArticle

admin.site.register(Article)
admin.site.register(Author)
admin.site.register(Highlight)
admin.site.register(Tag)
# admin.site.register(User)
admin.site.register(Activity)
admin.site.register(FollowedTag)
admin.site.register(FollowedArticle)

class AdminReportArticle(admin.ModelAdmin):
    list_display = ('pmid', 'report_message', 'report_date', 'is_open')


admin.site.register(ReportArticle, AdminReportArticle)

class AdminUser(admin.ModelAdmin):
    list_display = ('email', 'is_active')


admin.site.register(User, AdminUser)
