from django.urls import include, path
from frontend import views

urlpatterns = [
    path('', views.index, name='index'),
    path('article/', include('frontend.urls')),
    path('site-settings/', include('backend.urls')),
    path('user/', include('usermanagement.urls')),
]
