# syntax=docker/dockerfile:1

FROM python:3.9

# set work directory
WORKDIR /usr/src/app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install --upgrade pip

RUN apt-get update \
    && apt-get -y install libpq-dev gcc \
    && pip install psycopg2 \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    netcat

COPY requirements.txt .
RUN pip install -r requirements.txt

# copy entrypoint.sh
COPY ./entrypoint.sh /usr/src/app/entrypoint.sh

COPY . /usr/src/app

RUN ["chmod", "+x", "/usr/src/app/entrypoint.sh"]
# run entrypoint.sh
ENTRYPOINT ["/usr/src/app/entrypoint.sh"]
