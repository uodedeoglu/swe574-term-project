from django.contrib.postgres.search import SearchQuery, SearchVector
from django.core import paginator
from django.db.models.expressions import F
from django.shortcuts import render, redirect, get_object_or_404
from backend.models import *
from usermanagement.models import FollowedArticle, FollowedTag
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from frontend.forms import *
from backend.activityStreams import *
from django.contrib import messages
from django.http import JsonResponse
from datetime import datetime
import json as jsonn
from qwikidata.sparql import get_subclasses_of_item, return_sparql_query_results
from django.db import connection
from django.views.decorators.csrf import requires_csrf_token
import requests
import time
import threading
from django.core.mail import send_mail
from django.conf import settings
import re
from datetime import datetime as dt


paginator = None
filtered_articles = None
search_query = None
general_query = None
query = None
query_term = None
json = None
gl_sort_type = 0
resulting_list_of_articles = None

def index(request):
    return render(request, 'home.html', {'form': searchForm()})


def basic_search_result(request, sort_type=0):
    global filtered_articles
    global paginator
    global query_term
    global gl_sort_type 
    global search_query
    global resulting_list_of_articles

    if request.method == "POST":
        query_term = request.POST['keyword']
        if query_term:
            
            filtered_articles = Article.objects.filter(search_vector = query_term).distinct().values_list('pmid', 'title', 'publish_date') 
            
            just_to_fetch_query = filtered_articles.last()
            page_number = request.POST.get('page')
            if page_number == None:
                page_number=1

            query_searched(request, query_term)

            query_term_search_activities = Activity.objects.filter(JSON_Field__type = "Search", JSON_Field__query_term = query_term)
            search_count = query_term_search_activities.count()

            is_query_saved = Activity.objects.filter(JSON_Field__type= "QuerySaved", JSON_Field__query_term = query_term, JSON_Field__actor__name = str(request.user.id)).exists()

            resulting_list_of_articles = []
            for i in filtered_articles:
                resulting_list_of_articles.append(i)
                
            remainder_page = int(len(resulting_list_of_articles)%20 > 0)
            max_page_count = int(len(resulting_list_of_articles)/20) + len(resulting_list_of_articles)%20

            context_dict = {"filtered_articles": resulting_list_of_articles[20*int(page_number) -20:20*int(page_number)],
                            "query_term": query_term,
                            'form': searchForm(),
                            'sort_type': sort_type,
                            "search_count": search_count,
                            'is_query_saved': is_query_saved,
                            'real_length': range(max_page_count),
                            'page': page_number
                            }
            
            gl_sort_type = sort_type
            return render(request=request, template_name="searchResult.html", context=context_dict)
        else:
            return redirect('index')
    elif request.method == "GET":
        page_number = request.GET.get('page')
        
        if page_number == None:
            page_number=1

        if (sort_type==0) & (gl_sort_type != sort_type):
            filtered_articles = Article.objects.filter(
            Q(title__icontains=query_term) | Q(abstract__icontains=query_term)).distinct().values_list('pmid', 'title', 'publish_date')
            resulting_list_of_articles = []
            for i in filtered_articles:
                resulting_list_of_articles.append(i)
        elif (sort_type==1) & (gl_sort_type != sort_type):
            sortTuple(resulting_list_of_articles, sort_type)
        elif (sort_type==2) & (gl_sort_type != sort_type):
            sortTuple(resulting_list_of_articles, sort_type)
        elif (sort_type==3) & (gl_sort_type != sort_type):
            sortTuple(resulting_list_of_articles, sort_type)
        elif (sort_type==4) & (gl_sort_type != sort_type):
            sortTuple(resulting_list_of_articles, sort_type)

        query_term_search_activities = Activity.objects.filter(JSON_Field__type = "Search", JSON_Field__query_term = query_term)
        search_count = query_term_search_activities.count()

        is_query_saved = Activity.objects.filter(JSON_Field__type= "QuerySaved", JSON_Field__query_term = query_term, JSON_Field__actor__name = str(request.user.id)).exists()

        remainder_page = int(len(resulting_list_of_articles)%20 > 0)
        max_page_count = int(len(resulting_list_of_articles)/20) + remainder_page

        context_dict = {"filtered_articles": resulting_list_of_articles[20*int(page_number) -20:int(page_number)*20],
                        "query_term": query_term,
                        'form': searchForm(),
                        'sort_type': sort_type,
                        "search_count": search_count,
                        "is_query_saved": is_query_saved,
                        'real_length': range(max_page_count),
                        'page': page_number
                        }

        gl_sort_type = sort_type
        return render(request=request, template_name="searchResult.html", context=context_dict)


def show_article_detail(request, pmid):
    if request.user.is_authenticated:
        article = get_object_or_404(Article, pmid=pmid)
        article_displayed(request, article)

        article_display_activities = Activity.objects.filter(JSON_Field__type = "Display", JSON_Field__pmid = f"{pmid}")
        display_count = article_display_activities.count()

        return render(request, 'articleDetail.html', context={'article': article, 'followed': check_if_article_followed(request.user, pmid),'display_count': display_count})
    else:
        messages.error(request, 'Please login to view the details')
        return redirect('login')


def show_report_page(request, pmid):
    if request.method == "GET":
        if request.user.is_authenticated:
            return render(request, "reportPage.html", {'form': reportArticleForm(), 'pmid': pmid})
        else:
            return redirect('index')


def submit_report_page(request):
    if request.method == "POST":
        if request.user.is_authenticated:
            try:
                a = ReportArticle.objects.get_or_create(
                    pmid=request.POST['pmid'], user_report_id=request.user, report_message=request.POST['report_message'])
                messages.success(request, 'Report sent to Admin.')
                return redirect('index')
            except:
                messages.error(request, 'Report could not sent to Admin.')
        else:
            return redirect('index')

def show_annotate_page(request, pmid, sub_text=""):
    if request.method == "GET":
        if request.user.is_authenticated:
            if(sub_text != " " and len(sub_text)>=5):
                return render(request, "customAnnotatePage.html", {'form': customAnnotationForm(), 'sub_text': sub_text, 'pmid': pmid})
            else:
                messages.error(request, 'Could not annotate without selection, please select part of abstract, at least 5 character.')
                return redirect(request.META.get('HTTP_REFERER'))
        else:
            return redirect('index')

@requires_csrf_token
def submit_annotate_page(request):
    if request.method == "POST":
        if request.user.is_authenticated:
            try:
                b, _ = Tag.objects.get_or_create(label=request.POST['custom_annotation'])
                a, _ = Highlight.objects.get_or_create(tag=b, sub_text=request.POST['sub_text'])
                c = Article.objects.get(pmid=request.POST['pmid'])
                c.highlights.add(a)
                annotation_added(request, a,request.POST['pmid'])
                messages.success(request, 'Tag annotated.')
                return render(request, 'articleDetail.html', context={'article': c, 'followed': check_if_article_followed(request.user, c.pmid)})
            except:
                messages.error(request, 'Couldn\'t annotate')
                return redirect(request.META.get('HTTP_REFERER'))
        else:
            return redirect('index')

def advanced_search(request, sort_type=0):
    global paginator
    global filtered_articles
    global search_query
    global general_query
    global query
    global json
    global gl_sort_type
    global resulting_list_of_articles

    if request.method == "POST":
        if request.user.is_authenticated:

            temp_dict = {}
            temp_dict['min_date'] = request.POST["date_min"]
            temp_dict['max_date'] = request.POST["date_max"]

            if(temp_dict['min_date'] == ''):
                temp_dict['min_date'] = '2017-01-03'
            if(temp_dict['max_date'] == ''):
                temp_dict['max_date'] = '2022-12-30'

            try:
                temp_dict['is_vector'] = request.POST["vector-search"]
            except Exception as e:
                temp_dict['is_vector'] = ''

            temp_list = []

            counter = 0
            while True:
                temp_subdict = {}

                if counter > 0:
                    try:
                        if(len(request.POST['article-part' + str(counter)]) > 0):
                            temp_subdict['field'] = request.POST['article-part' + str(counter)]
                            temp_subdict['search_terms'] = request.POST['search-terms' + str(counter)]
                            temp_subdict['is_combined'] = request.POST['search-option' + str(counter)]
                    except Exception as ex:
                        break
                else:
                    temp_subdict['field'] = request.POST['article-part']
                    temp_subdict['search_terms'] = request.POST['search-terms']
                    temp_subdict['is_combined'] = request.POST['search-option']

                if temp_subdict["is_combined"] == 'separated':
                    temp_subdict["is_combined"] = False
                else:
                    temp_subdict["is_combined"] = True

                temp_list.append(temp_subdict)
                counter = counter + 1

            temp_dict['search_items'] = temp_list
            json = temp_dict

            min_date = json['min_date']
            max_date = json['max_date']
            search_items = json['search_items']

            article_parts = ['title', 'abstract',  'keywords', 'authors', 'tags']

            try:

                general_query = Q()
                general_query.add(Q(publish_date__gte=datetime.strptime(
                    min_date, '%Y-%m-%d')), general_query.connector)
                general_query.add(Q(publish_date__lte=datetime.strptime(
                    max_date, '%Y-%m-%d')), general_query.connector)

                vector_query = []
                for search_item in search_items:
                    query = Q()
                    if search_item['is_combined']:
                        if search_item['field'] == 'all':
                            for part in article_parts:
                                if part == 'authors':
                                    query.add(
                                        Q(**{"{}__icontains".format(part + "__name"): search_item['search_terms']}), Q.OR)
                                elif part == 'tags':
                                    query.add(
                                        Q(**{"{}__icontains".format(part + "__label"): search_item['search_terms']}), Q.OR)
                                    query.add(
                                        Q(**{"{}__icontains".format(part + "__custom_description"): search_item['search_terms']}), Q.OR)
                                else:
                                    query.add(
                                        Q(**{"{}__icontains".format(part): search_item['search_terms']}), Q.OR)
                        else:
                            if search_item['field'] == 'authors':
                                query.add(
                                    Q(**{"{}__icontains".format(search_item['field'] + "__name"): search_item['search_terms']}), Q.OR)
                            elif search_item['field'] == 'tags':
                                query.add(
                                    Q(**{"{}__icontains".format(search_item['field'] + "__label"): search_item['search_terms']}), Q.OR)
                                query.add(
                                    Q(**{"{}__icontains".format(search_item['field'] + "__custom_description"): search_item['search_terms']}), Q.OR)
                            else:
                                query.add(
                                    Q(**{"{}__icontains".format(search_item['field']): search_item['search_terms']}), Q.OR)
                        
                        for key in search_item['search_terms'].split():
                            vector_query.append(key)

                    else:
                        keys = search_item['search_terms'].split()
                        if search_item['field'] == 'all':
                            for part in article_parts:
                                for key in keys:
                                    if part == 'authors':
                                        vector_query.append(key)
                                        query.add(
                                            Q(**{"{}__icontains".format(part + "__name"): key}), Q.OR)
                                    elif part == 'tags':
                                        query.add(
                                            Q(**{"{}__icontains".format(part + "__label"): key}), Q.OR)
                                        query.add(
                                            Q(**{"{}__icontains".format(part + "__custom_description"): key}), Q.OR)
                                    else:
                                        query.add(
                                            Q(**{"{}__icontains".format(part): key}), Q.OR)
                        else:
                            for key in keys:
                                vector_query.append(key)
                                if search_item['field'] == 'authors':
                                    query.add(
                                        Q(**{"{}__icontains".format(search_item['field'] + "__name"): key}), Q.OR)
                                elif search_item['field'] == 'tags':
                                    query.add(
                                        Q(**{"{}__icontains".format(search_item['field'] + "__label"): key}), Q.OR)
                                    query.add(
                                        Q(**{"{}__icontains".format(search_item['field'] + "__custom_description"): key}), Q.OR)
                                else:
                                    query.add(
                                        Q(**{"{}__icontains".format(search_item['field']): key}), Q.OR)
                    
                    general_query.add(query, Q.AND)
                    
                if json['is_vector'] == 'on':   
                    for item in vector_query:
                        query.add(Q(**{"{}__icontains".format('search_vector'): item}), Q.OR)
                    
                general_query.add(query, conn_type=general_query.connector)
                
                query = ""
                counter = 0
                for item in vector_query:
                    if counter != 0:
                        query = query + '| '
                    query = query + item + ' '
                    counter = counter + 1
               
                if json['is_vector'] == 'on':
                    search_query = SearchQuery(query, search_type="raw")
                    filtered_articles = Article.objects.filter(general_query).annotate(rank=SearchVector(F('search_vector'), search_query)).distinct().order_by('-rank').values_list('pmid', 'title', 'publish_date')
                else:
                    filtered_articles = Article.objects.filter(general_query).distinct().values_list('pmid', 'title', 'publish_date')
                
                just_to_fetch_query = filtered_articles.last()
                
                page_number = request.POST.get('page')
                if page_number == None:
                    page_number=1

                query_searched(request, str(json))

                query_term_search_activities = Activity.objects.filter(JSON_Field__type = "Search", JSON_Field__query_term = str(json))
                search_count = query_term_search_activities.count()

                is_query_saved = Activity.objects.filter(JSON_Field__type= "QuerySaved", JSON_Field__query_term = str(json), JSON_Field__actor__name = str(request.user.id)).exists()

                resulting_list_of_articles = []
                for i in filtered_articles:
                    resulting_list_of_articles.append(i)
                    
                remainder_page = int(len(resulting_list_of_articles)%20 > 0)
                max_page_count = int(len(resulting_list_of_articles)/20) + len(resulting_list_of_articles)%20

                context_dict = {"filtered_articles": resulting_list_of_articles[20*int(page_number) -20:20*int(page_number)],
                                "query_term": str(json),
                                'form': searchForm(),
                                'sort_type': sort_type,
                                'search_count': search_count,
                                'is_query_saved': is_query_saved,
                                'real_length': range(max_page_count),
                                'page': page_number
                                }

                gl_sort_type = sort_type
                return render(request=request, template_name="searchResult.html", context=context_dict)

            except Exception as ex:
                template = "An exception of type {0} occurred. Arguments:\n{1!r}"
                message = template.format(type(ex).__name__, ex.args)
                print(message)
                return redirect('index')

    elif request.method == "GET":
        if request.user.is_authenticated:
            page_number = request.GET.get('page')
            if page_number == None:
                page_number=1

            if (sort_type==0) & (gl_sort_type != sort_type):
                filtered_articles = Article.objects.filter(general_query).annotate(rank=SearchVector(F('search_vector'), search_query)).distinct().order_by('-rank').values_list('pmid', 'title', 'publish_date')
                resulting_list_of_articles = []
                for i in filtered_articles:
                    resulting_list_of_articles.append(i)
            elif (sort_type==1) & (gl_sort_type != sort_type):
                resulting_list_of_articles = sortTuple(resulting_list_of_articles, sort_type)
            elif (sort_type==2) & (gl_sort_type != sort_type):
                resulting_list_of_articles = sortTuple(resulting_list_of_articles, sort_type)
            elif (sort_type==3) & (gl_sort_type != sort_type):
                resulting_list_of_articles = sortTuple(resulting_list_of_articles, sort_type)
            elif (sort_type==4) & (gl_sort_type != sort_type):
                resulting_list_of_articles = sortTuple(resulting_list_of_articles, sort_type)

            query_term_search_activities = Activity.objects.filter(JSON_Field__type = "Search", JSON_Field__query_term = str(json))
            search_count = query_term_search_activities.count()

            is_query_saved = Activity.objects.filter(JSON_Field__type= "QuerySaved", JSON_Field__query_term = str(json), JSON_Field__actor__name = str(request.user.id)).exists()

            remainder_page = int(len(resulting_list_of_articles)%20 > 0)
            max_page_count = int(len(resulting_list_of_articles)/20) + remainder_page
            
            context_dict = {"filtered_articles": resulting_list_of_articles[20*int(page_number) -20:int(page_number)*20],
                            "query_term": str(json),
                            'form': searchForm(),
                            'sort_type': sort_type,
                            'search_count': search_count,
                            'is_query_saved': is_query_saved,
                            'real_length': range(max_page_count),
                            'page': page_number
                            }

            gl_sort_type = sort_type
            return render(request=request, template_name="searchResult.html", context=context_dict)

def partition_title_az(arr, low, high):
    i = (low-1)         # index of smaller element
    pivot = arr[high]     # pivot
 
    for j in range(low, high):
 
        # If current element is smaller than or
        # equal to pivot
        if compareAlphanumeric(arr[j][1], pivot[1]):
            
 
            # increment index of smaller element
            i = i+1
            arr[i], arr[j] = arr[j], arr[i]
 
    arr[i+1], arr[high] = arr[high], arr[i+1]
    return (i+1)

def partition_title_za(arr, low, high):
    i = (low-1)         # index of smaller element
    pivot = arr[high]     # pivot
 
    for j in range(low, high):
 
        # If current element is smaller than or
        # equal to pivot
        if not compareAlphanumeric(arr[j][1], pivot[1]):
            
 
            # increment index of smaller element
            i = i+1
            arr[i], arr[j] = arr[j], arr[i]
 
    arr[i+1], arr[high] = arr[high], arr[i+1]
    return (i+1)

def partition_date_asc(arr, low, high):
    i = (low-1)         # index of smaller element
    pivot = arr[high]     # pivot
 
    for j in range(low, high):
 
        # If current element is smaller than or
        # equal to pivot
        if arr[j][2] <= pivot[2]:
            
            # increment index of smaller element
            i = i+1
            arr[i], arr[j] = arr[j], arr[i]
 
    arr[i+1], arr[high] = arr[high], arr[i+1]
    return (i+1)

def partition_date_des(arr, low, high):
    i = (low-1)         # index of smaller element
    pivot = arr[high]     # pivot
 
    for j in range(low, high):
 
        # If current element is smaller than or
        # equal to pivot
        if arr[j][2] > pivot[2]:
            
            # increment index of smaller element
            i = i+1
            arr[i], arr[j] = arr[j], arr[i]
 
    arr[i+1], arr[high] = arr[high], arr[i+1]
    return (i+1)

def quickSort(arr, low, high, sort_type):
    if len(arr) == 1:
        return arr
    if low < high:
 
        # pi is partitioning index, arr[p] is now
        # at right place
        if sort_type == 3:
            pi = partition_title_az(arr, low, high)
        elif sort_type == 4:
            pi = partition_title_za(arr, low, high)
        elif sort_type == 2:
            pi = partition_date_asc(arr, low, high)
        else:
            pi = partition_date_des(arr, low, high)
 
        # Separately sort elements before
        # partition and after partition
        quickSort(arr, low, pi-1, sort_type)
        quickSort(arr, pi+1, high, sort_type)
        
# Function to check alphanumeric equality
# of both strings and return true if 1 < 2
def compareAlphanumeric(str1, str2):
     
    # variable declaration
    i = 0
    j = 0
 
    # Length of first string
    len1 = len(str1)
 
    # Length of second string
    len2 = len(str2)
 
    # To check each and every character of both string
    while (i < len1 and j < len2):
         
        # If the current character of the first string
        # is not an alphanumeric character,
        # increase the pointer i
        while (i < len1 and 
              (((str1[i] >= 'a' and str1[i] <= 'z') or
                (str1[i] >= 'A' and str1[i] <= 'Z') or
                (str1[i] >= '0' and str1[i] <= '9')) == False)):
            i += 1
 
        # If the current character of the second string
        # is not an alphanumeric character,
        # increase the pointer j
        while (j < len2 and 
              (((str2[j] >= 'a' and str2[j] <= 'z') or
                (str2[j] >= 'A' and str2[j] <= 'Z') or
                (str2[j] >= '0' and str2[j] <= '9')) == False)):
            j += 1


 
        # if any alphanumeric characters of
        # both strings are not same, then return check for precedence
        if (str1[i-1] != str2[j-1]):
            if (str1[i].lower() <= str2[j].lower()):
                return True
            else:
                return False
 
        # If current character matched,
        # increase both pointers
        # to check the next character
        else:
            i += 1
            j += 1
 
    return len1 < len2

def sortTuple(tup, sort_type):
    if sort_type == 1:
        # Getting the length of list 
        # of tuples
        n = len(tup)
        quickSort(tup, 0, n-1, sort_type)
                    
        return tup
    elif sort_type == 2:
        # Getting the length of list 
        # of tuples
        n = len(tup)
        quickSort(tup, 0, n-1, sort_type)
                    
        return tup
    elif sort_type == 3:
        # Getting the length of list 
        # of tuples
        n = len(tup)
        n = len(tup)
        quickSort(tup, 0, n-1, sort_type)
                    
        return tup
    else :
        # Getting the length of list 
        # of tuples
        n = len(tup)
        quickSort(tup, 0, n-1, sort_type)
                    
        return tup 

def navigate_advanced_search(request):
    article_parts = ['all', 'title', 'abstract', 'authors', 'keywords', 'tags']

    search_options = ['separated', 'combined']

    context_dict = {'article_parts': article_parts,
                    'search_option': search_options  # ,
                    # 'form': advancedSearchForm()
                    }

    return render(request, 'advancedSearch.html', context=context_dict)


def show_custom_tag_page(request, pmid):
    if request.method == "GET":
        if request.user.is_authenticated:
            return render(request, "customTagPage.html", {'form': customTagForm(), 'pmid': pmid})
        else:
            return redirect('index')


def submit_custom_tag(request):
    if request.method == "POST":
        if request.user.is_authenticated:
            try:
                tag, _ = Tag.objects.get_or_create(custom_description=request.POST['custom_tag'])
                article = Article.objects.get(pmid=request.POST['pmid'])
                article.tags.add(tag)
                article.save()

                th1 = threading.Thread(target=populate_search_vector)
                th1.start()

                messages.success(request, 'Custom tag added successfully')
                return_article = get_object_or_404(Article, pmid=request.POST['pmid'])
                tag_added(request, tag, return_article.pmid)
                return render(request, 'articleDetail.html', context={'article': return_article, 'followed': check_if_article_followed(request.user, return_article.pmid)})

            except:
                messages.error(request, 'Error with custom tagging')
        else:
            return redirect('index')


def search_tag(request):
    if request.method == "GET":
        if request.user.is_authenticated:
            return render(request, {'form': tagSeach()})
        else:
            return redirect('index')
    else:
        tag_article(request.POST['pmid'])
        messages.success(request, 'Article is tagged')
        return redirect('index')


@requires_csrf_token
def tag_article(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            body_unicode = request.body.decode('utf-8')
            body_data = jsonn.loads(body_unicode)
            article_information = Article.objects.get(pmid=body_data['pmid'])

            for tag in body_data['tags']:
                try:

                    current_wiki_url = "https://www.wikidata.org/wiki/" +  str(tag["tagId"])

                    tag, _ = Tag.objects.get_or_create(wiki_q=str(tag["tagId"]), label=str(tag["tag"]),
                                                 wiki_description=str(tag["tagDescription"]), wiki_url=current_wiki_url,
                                                 custom_description=str(tag["tagAlias"]))

                    th = threading.Thread(target=save_related_object_fields, args=[tag,tag.id])
                    th.start()

                    article_information.tags.add(tag)
                    article_information.save()
                    tag_added(request, tag, article_information.pmid)
                    messages.success(request, 'Article tagging is successfull')

                except Exception as x:
                    print("Exception", x)
                    return JsonResponse({'status': 'Failed'})

            return JsonResponse({'status': 'Success'})


@requires_csrf_token
def annotate_article(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            body_unicode = request.body.decode('utf-8')
            body_data = jsonn.loads(body_unicode)
            article_information = Article.objects.get(pmid=body_data['pmid'])
            sub_text = body_data['sub_text']

            for tag in body_data['tags']:
                try:
                    current_wiki_url = "https://www.wikidata.org/wiki/" +  str(tag["tagId"])
                    tag, _ = Tag.objects.get_or_create(wiki_q=str(tag["tagId"]), label=str(tag["tag"]),
                                                 wiki_description=str(tag["tagDescription"]), wiki_url=current_wiki_url,
                                                 custom_description=str(tag["tagAlias"]))

                    highlight, _ = Highlight.objects.get_or_create(sub_text=sub_text, tag=tag)

                    th = threading.Thread(target=save_related_object_fields, args=[tag,tag.id])
                    th.start()

                    article_information.highlights.add(highlight)
                    article_information.save()
                    annotation_added(request, highlight, article_information.pmid)
                    messages.success(request, 'Annotation is successfull')

                except Exception as x:
                    print("Exception", x)
                    return JsonResponse({'status': 'Failed'})

            return JsonResponse({'status': 'Success'})


def show_profile(request):
    if request.user.is_authenticated:
        user_email = request.user.email
        user_info = User.objects.get(email=user_email)
        return render(request, 'profile.html', context={'user_info': user_info})
    else:
        return redirect('loginuser')


def show_feed(request):
    if request.user.is_authenticated:
        user_email = request.user.email
        user_info = User.objects.get(email=user_email)

        activities = get_activity(request)

        # summaries = []
        # published_dates = []
        new_activities = []

        context_dict = {"user_info": user_info,
                        # "activities": activities,
                        # 'summaries': summaries,
                        # 'published_dates': published_dates,
                        'new_activities': new_activities
                        }

        for new_activity in activities:
            temp = jsonn.loads(new_activity)
            new_activities.append(temp)

        # for single_activity in activities:
        #     temp = json.loads(single_activity)
        #     summaries.append(temp["summary"])
        #     published_dates.append(temp["published"])

        return render(request, 'feed.html', context=context_dict)
    else:
        return redirect('loginuser')


def show_user_lookup(request):
    if request.user.is_authenticated:
        user_email = request.user.email
        user_info = User.objects.get(email=user_email)
        return render(request, 'userLookup.html', {'form': userSearchForm(), 'user_info': user_info})
    else:
        return redirect('loginuser')


def show_tag_lookup(request):
    if request.user.is_authenticated:
        user_email = request.user.email
        user_info = User.objects.get(email=user_email)
        return render(request, 'tagLookup.html', {'form': tagSearchForm(), 'user_info': user_info})
    else:
        return redirect('loginuser')


def follow_article(request, pmid):
    followed_article, __ = FollowedArticle.objects.get_or_create(pmid=pmid)
    followed_article.save()

    user_email = request.user.email
    user = User.objects.get(email=user_email)

    user.followed_articles.add(followed_article)
    user_follow(user_email, str(pmid), "Article", request.user.name, str(pmid))

    return_article = get_object_or_404(Article, pmid=pmid)

    return render(request, 'articleDetail.html', context={'article': return_article, 'followed': check_if_article_followed(request.user, pmid)})


def unfollow_article(request, pmid):
    user_email = request.user.email
    user = User.objects.get(email=user_email)

    followed_article = get_object_or_404(FollowedArticle, pmid=pmid)

    user.followed_articles.remove(followed_article)
    user_unfollow(user_email, str(pmid), "Article", user.name, str(pmid))

    return_article = get_object_or_404(Article, pmid=pmid)
    return render(request, 'articleDetail.html', context={'article': return_article, 'followed': check_if_article_followed(request.user, pmid)})


def follow_tag(request, unique_id):
    followed_tag, __ = FollowedTag.objects.get_or_create(unique_id=unique_id)
    followed_tag.save()

    tag_object = Tag.objects.get(unique_id=unique_id)
    if tag_object.label != "":
        tag_name = tag_object.label
    else:
        tag_name = tag_object.custom_description

    user_email = request.user.email
    user = User.objects.get(email=user_email)

    user.followed_tags.add(followed_tag)
    user_follow(user_email, str(unique_id), "Tag", user.name, tag_name)

    messages.success(request, 'Tag successfully followed')

    return redirect('index')

    # return render(request, 'tagLookup.html', {'form': tagSearchForm(), 'user_info': user})


def unfollow_tag(request, unique_id):
    user_email = request.user.email
    user = User.objects.get(email=user_email)

    followed_tag = get_object_or_404(FollowedTag, unique_id=unique_id)
    tag_object = Tag.objects.get(unique_id=unique_id)

    if tag_object.label != "":
        tag_name = tag_object.label
    else:
        tag_name = tag_object.custom_description

    user.followed_tags.remove(followed_tag)
    user_unfollow(user_email, str(unique_id), "Tag", user.name, tag_name)

    messages.warning(request, 'Tag successfully unfollowed')

    return redirect('index')

    # return render(request, 'tagLookup.html', {'form': tagSearchForm(), 'user_info': user})


def follow_user(request, email):
    followed_user = User.objects.get(email=email)
    following_user = User.objects.get(email=request.user.email)

    following_user.followed_users.add(followed_user)
    user_follow(request.user.email, email, "User", request.user.name, followed_user.name)

    messages.success(request, 'User successfully followed')

    return redirect('index')

    # return render(request, 'userLookup.html', {'form': userSearchForm(), 'user_info': user})


def unfollow_user(request, email):
    followed_user = User.objects.get(email=email)
    following_user = User.objects.get(email=request.user.email)

    following_user.followed_users.remove(followed_user)
    user_unfollow(request.user.email, email, "User", request.user.name, followed_user.name)

    messages.warning(request, 'User successfully unfollowed')

    return redirect('index')

    # return render(request, 'userLookup.html', {'form': userSearchForm(), 'user_info': user})


def search_user_lookup(request):
    if request.method == "GET":
        if 'user_search_field' in request.GET:
            query_term = request.GET['user_search_field']
            if query_term:
                search_results = User.objects.filter(
                    Q(name__icontains=query_term)).distinct()
                page_number = request.GET.get('page')
                paginator = Paginator(search_results, 20)

                try:
                    objects = paginator.page(page_number)
                except PageNotAnInteger:
                    objects = paginator.page(1)
                except EmptyPage:
                    objects = paginator.page(paginator.num_pages)

                context_dict = {"filtered_users": objects,
                                "followed_users": return_followed_users(request.user),
                                "query_term": query_term,
                                'form': userSearchForm()
                                }

            return render(request=request, template_name="userSearchResult.html", context=context_dict)
        else:
            return redirect('index')


def search_tag_lookup(request):
    if request.method == "GET":
        if 'tag_search_field' in request.GET:
            query_term = request.GET['tag_search_field']
            if query_term:
                search_results = Tag.objects.filter(Q(label__icontains=query_term) | Q(
                    custom_description__icontains=query_term)).distinct()
                page_number = request.GET.get('page')
                paginator = Paginator(search_results, 20)

                try:
                    objects = paginator.page(page_number)
                except PageNotAnInteger:
                    objects = paginator.page(1)
                except EmptyPage:
                    objects = paginator.page(paginator.num_pages)

                context_dict = {"filtered_tags": objects,
                                "followed_tags": return_followed_tags(request.user),
                                "query_term": query_term,
                                'form': tagSearchForm()
                                }

            return render(request=request, template_name="tagSearchResult.html", context=context_dict)
        else:
            return redirect('index')


def check_if_article_followed(user, pmid):
    pmid = str(pmid)
    for followed_article in user.followed_articles.all():
        if followed_article.pmid == pmid:
            return True

    return False


def return_followed_tags(user):
    followed_tags = []
    for followed_tag in user.followed_tags.all():
        followed_tags.append(followed_tag.unique_id)

    return followed_tags


def return_followed_users(user):
    followed_users = []
    for followed_user in user.followed_users.all():
        followed_users.append(followed_user.email)

    return followed_users


def populate_search_vector():
    try:
        cursor = connection.cursor()
        cursor.execute("""select "search_vector_update"();;""")
    except Exception as e:
        print(e)
        cursor.close


def save_related_object_fields(tag, uuid):
    try:
        related_objects = get_related_ojects(tag)
        similar_entities = get_similar_entities(tag)

        Tag.objects.filter(id = tag.id).update(related_wiki_objects = related_objects,
                                                    similar_wiki_objects = similar_entities)

        th1 = threading.Thread(target=populate_search_vector)
        th1.start()

    except Exception as e:
        print("Exception", e)

def get_related_ojects(tag):
    try:
        sparql_query = """
            SELECT DISTINCT ?item ?itemLabel
            WHERE {
            wd:""" + str(tag.wiki_q) +"""(wdt:P31 | wdt:P279 | wdt:P780 | wdt:P361 ) ?item
            SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
            }
            LIMIT 5
            """

        res = return_sparql_query_results(sparql_query)
        result_list = res['results']['bindings']

        related_items = []
        for result in result_list:
            related_items.append(result['itemLabel']['value'])

        related_items = ",".join(related_items)
    except Exception as e:
        print("Exception", e)
        related_items = ''
    return related_items

def get_similar_entities(tag):
    try:
        wiki_id = tag.wiki_q
        response = requests.get('https://wembedder.toolforge.org/api/most-similar/' + str(wiki_id))
        response_data = response.json()
        similar_objects_list = []
        label_list = []

        embedding_list = response_data['most_similar']
        embedding_list = embedding_list[5:]
        for embedding in embedding_list:
            temp_response = requests.get('https://www.wikidata.org/w/api.php?action=wbsearchentities&search=' + embedding['item'] + '&language=en&format=json')
            temp_response = temp_response.json()
            label = temp_response['search'][0]['label']
            similar_objects_list.append(embedding['item'])
            label_list.append(label)

        label_list = ",".join(label_list)
    except Exception as e:
        print("Exception", e)
        label_list = ''

    return label_list

def show_condition_of_use(request):
    return render(request, 'conditionOfUse.html')

def show_privacy_notice(request):
    return render(request, 'privacyNotice.html')

def show_signup_request(request):

    new_users = User.objects.filter(is_active=False)
    new_users_flag = False
    if new_users.exists():
        new_users_flag = True
    else:
        new_users_flag = False

    return render(request, 'signupRequest.html', context={'new_users': new_users, 'new_users_flag': new_users_flag})

def approve_user_request(request, email):
    approve_user = User.objects.get(email=email)
    approve_user.is_active = True
    approve_user.save()
    new_users = User.objects.filter(is_active=False)
    new_users_flag = False
    if new_users.exists():
        new_users_flag = True
    else:
        new_users_flag = False
    try:
        send_mail(subject='Welcome to MediPub', message='User registration request accepted by Medipub \nYou may click to start your journey \nhttp://3.91.234.233:8000/ ', from_email=settings.EMAIL_HOST_USER, recipient_list=[email])
    except:
        print("Email service error")
    return render(request, 'signupRequest.html', context={'new_users': new_users, 'new_users_flag': new_users_flag})

def reject_user_request(request, email):
    reject_user = User.objects.get(email=email)
    reject_user.delete()
    new_users = User.objects.filter(is_active=False)
    new_users_flag = False
    if new_users.exists():
        new_users_flag = True
    else:
        new_users_flag = False
    print(new_users)
    if new_users_flag == True:
        print('Flag: True')
    if new_users_flag == False:
        print('Flag: False')
    try:
        send_mail(subject='Sorry, We cant accept your application', message='User registration request rejected by Medipub', from_email=settings.EMAIL_HOST_USER, recipient_list=[email])
    except:
        print("Email service error")
    return render(request, 'signupRequest.html', context={'new_users': new_users, 'new_users_flag': new_users_flag})

def save_query(request):
    if request.method == "POST":
        if 'query_term' in request.POST:
            query_term = request.POST['query_term']
            query_saved(request, query_term)

        if 'source_url' in request.POST:
            source_url = request.POST['source_url']
            return redirect(source_url)

def delete_query(request):
    if request.method == "POST":
        if 'query_term' in request.POST:
            query_term = request.POST['query_term']
            query_deleted(request, query_term)

        if 'source_url' in request.POST:
            source_url = request.POST['source_url']
            return redirect(source_url)
