$(document).ready(function () {
    var addedItems = [];
    var addedPmId = "";
    var selectedText = "";
    var isRowValid = true;

    $('[data-toggle="popover"]').popover({
        container: 'body',
        html: true
    });

    $("span.abstract").each(function () {
        var theContent = $(this).html();
        if (theContent.length > 400) {
            var n = theContent.substr(0, 300);
            var n2 = theContent.substr(-(theContent.length - 300))
            $(this).html("<span>" + n + '...</span> <span class="continue" style="display:none">' + n2 + '   </span>');
        } else {
            $(this).next().css("display", "none")
        }
    });

    $(".readControl-btn").on('click', function () {
        if ($(this).html() == "Read More") {
            $(this).html("Read Less");
            var fullAbstract = ''
            $(this).prev().children().each(function () {
                fullAbstract += $(this).html().slice(0, -3)
            })
            $(this).prev().html(fullAbstract)
        } else if ($(this).html() == "Read Less") {
            $(this).html("Read More")
            var theContent = $(this).prev().html()
            var n = theContent.substr(0, 300);
            var n2 = theContent.substr(-(theContent.length - 300))
            $(this).prev().html("<span>" + n + '...</span> <span class="continue" style="display:none">' + n2 + '   </span>')
        }
    });
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
        return false;
    };

    //////////////////// Advanced Search Part////////////////////
    $("#add_row").on("click", function () {

        // Get max row id and set new id
        var newid = 0;
        $.each($("#tab_logic tr"), function () {
            if (parseInt($(this).data("id")) > newid) {
                newid = parseInt($(this).data("id"));
            }
        });


        $.each($("#tab_logic tr"), function () {
            if (parseInt($(this).data("id")) == newid) {

                console.log($(this).find("td #article-part").find("option:selected").index())
                var selected_item_index = $(this).find("td #article-part").find("option:selected").index()

                console.log($(this).find("td #search-option").find("option:selected").index())
                var selected_option_index = $(this).find("td #search-option").find("option:selected").index()

                console.log($(this).find("td .search-terms").val())
                var search_terms = $(this).find("td .search-terms").val()

                if (search_terms.length == 0 | selected_item_index <= 0 | selected_option_index <= 0) {
                    console.log('here')
                    isRowValid = false;
                    alert("There are empty places in the row!!")
                    return;
                }
            }
        });

        if (!isRowValid) {
            isRowValid = true;
            return;
        }

        //return;  

        newid++;

        var tr = $("<tr></tr>", {
            id: "addr" + newid,
            "data-id": newid
        });

        // loop through each td and create new elements with name of newid
        $.each($("#tab_logic tbody tr:nth(0) td"), function () {
            var td;
            var cur_td = $(this);
            var children = cur_td.children();

            // add new td and element if it has a nane
            if ($(this).data("name") !== undefined) {
                td = $("<td></td>", {
                    "data-name": $(cur_td).data("name")
                });

                var c = $(cur_td).find($(children[0]).prop('tagName')).clone().val("");
                c.attr("name", $(cur_td).data("name") + newid);
                c.appendTo($(td));
                td.appendTo($(tr));
            } else {
                td = $("<td></td>", {
                    'text': $('#tab_logic tr').length
                }).appendTo($(tr));
            }
        });

        $("<td></td>").append(
            $("<button class='btn btn-danger glyphicon glyphicon-remove row-remove'>Remove</button>")
                .click(function () {
                    $(this).closest("tr").remove();
                })
        ).appendTo($(tr));

        // add the new row
        $(tr).appendTo($('#tab_logic'));

        //   $(tr).find("td button.row-remove").on("click", function() {
        //     $(this).closest("tr").remove();
        //   });
    });

    function validateRow(element) {
        let condition = true
        return condition;
    }

    //////////////////////////////////////////  TAGGING ////////////////////////////////////////////////////

    $('#id_keyword').val(getUrlParameter('keyword') != false ? getUrlParameter('keyword') : '')
    function getCookie(c_name) {
        if (document.cookie.length > 0) {
            c_start = document.cookie.indexOf(c_name + "=");
            if (c_start != -1) {
                c_start = c_start + c_name.length + 1;
                c_end = document.cookie.indexOf(";", c_start);
                if (c_end == -1) c_end = document.cookie.length;
                return unescape(document.cookie.substring(c_start, c_end));
            }
        }
        return "";
    }
    // Highlight
    $('.Full').on('click', (e) => {
        selectedText = window.getSelection().toString();
        var repl = RegExp('article/annotate/(.*?)/', 'g');
        console.log($('#custom_annotation_button').attr('href'))
        var length = $('#custom_annotation_button').attr('href').match(repl)[0].length;
        console.log($('#custom_annotation_button').attr('href').substring(0, length) + "/" + selectedText);
        $('#custom_annotation_button').attr('href', $('#custom_annotation_button').attr('href').substring(0, length) + "/" + selectedText);

        if (selectedText.length >= 5) {
            $('#msgNoAnnotationTarget').hide()
        }
        else{
            $('#msgNoAnnotationTarget').show()
        }
    });

    function hiliter(word, element) {
        var repl = '<span class="yellow-background">' + word + '</span>';
        element[0].innerHTML = element[0].innerHTML.replace(word, repl);
    }

    function unhiliter(word, element) {
        var repl = '<span class="yellow-background">' + word + '</span>';
        element[0].innerHTML = element[0].innerHTML.replace(repl, word);
    }

    $(".tag-button").hover(function (e) {
        $('#divTagDescription')
        .show()
        .text(e.target.id)
        .css({ position: 'absolute', color: '#000',
            left: e.pageX+15, top: e.pageY-35
        });
        console.log(e.target.id)
    }, function () {
        $('#divTagDescription')
        .hide()
    });

    $(".annotation-button").hover(function () {
        var sub_text_of_relation = this.id.substring(7);
        hiliter(sub_text_of_relation, $('.Full'));
    }, function () {
        var sub_text_of_relation = this.id.substring(7);
        unhiliter(sub_text_of_relation, $('.Full'));
    });
    //
    function delay(fn, ms) {

        let timer = 0
        return function (...args) {
            $("#results-" + addedPmId).html('<i class="fa fa-refresh fa-spin fa-x fa-fw"></i>')
            clearTimeout(timer)
            timer = setTimeout(fn.bind(this, ...args), ms || 0)
        }
    }

    $(".tagInput").keyup(delay(function (e) {
        let endpoint = "https://www.wikidata.org/w/api.php?action=wbsearchentities&"
        let config = "&language=en&limit=5&format=json"
        //var pressed = String.fromCharCode(e.which);
        var currentContent = $("#tagInput-" + addedPmId).val();

        $.ajax({
            url: endpoint + "search=" + currentContent + config,
            contentType: "application/json",
            crossDomain: true,
            dataType: 'jsonp',
            success: function (result) {
                $("#results-" + addedPmId).empty()
                $.each(result.search, function (index, value) {
                    if (value.match.type == "alias") {
                        var header = value.label + " (" + value.match.text + ")"
                        var content = value.description
                    }
                    else if (value.match.type == "label") {
                        var header = value.label
                        var content = value.description
                    }
                    $("#results-" + addedPmId).append('<a class="list-group-item list-group-item-action flex-column  align-items-start" data-tag-id="' + value.id + '" data-tag="' + header + '" data-tag-description="' + content + '"> \
            <div class="d-flex w-100 justify-content-between"> \
              <p class="mb-1 font-weight-bold">'+ header + '</p>\
            </div> \
            <p class="mb-1">'+ content + '</p> \
          </a>  ')
                });
            },
        })
    }, 500));

    //$(document).on('click', '#results-'+addedPmId+'.a', function (e) {
    $(".results").on('click', 'a', function (e) {
        $('#selectedGroupAll-' + addedPmId).show()
        if (!addedItems.some(item => item.tagId == e.currentTarget.dataset.tagId)) {
            var button_text_tag = $(".startTagging").text();
            var button_text_annotate = $(".startAnnotating").text();
            console.log(button_text_tag, "\n", button_text_annotate, "1");
            if (button_text_tag == "Save" && button_text_annotate != "Save") {
                $('#tagDetail').css('display', 'block')
                $('#modalTagName').html(e.currentTarget.dataset.tag)
                $('#modalTagDesc').html(e.currentTarget.dataset.tagDescription)
                $('#tagDetailTag').attr('data-tag-id', e.currentTarget.dataset.tagId);
                $('#tagDetailTag').attr('data-tag', e.currentTarget.dataset.tag);
                $('#tagDetailTag').attr('data-tag-description', e.currentTarget.dataset.tagDescription);
                $('#tagDetailAddAllias').css('display', 'inline');
                $('.alliasDiv').css('display', 'none');
                $('#modalTagAlias').val("")
            }
            else {
                if (selectedText != null && selectedText != "" && selectedText.length > 5) {
                    $('msgNoAnnotationTarget').hide()
                    $('#modalSubText').html(selectedText)
                    $('#annotateDetail').css('display', 'block')
                    $('#modalAnnotateTagName').html(e.currentTarget.dataset.tag)
                    $('#modalAnnotateTagDesc').html(e.currentTarget.dataset.tagDescription)
                    $('#annotateDetailTag').attr('data-tag-id', e.currentTarget.dataset.tagId);
                    $('#annotateDetailTag').attr('data-tag', e.currentTarget.dataset.tag);
                    $('#annotateDetailTag').attr('data-tag-description', e.currentTarget.dataset.tagDescription);
                    $('#annotateDetailAddAllias').css('display', 'inline');
                    $('.alliasDiv').css('display', 'none');
                    $('#modalAnnotateTagAlias').val("")
                }
                else {
                    $('msgNoAnnotationTarget').show()
                }
            }
        }
    });

    $('.selectedGroup').on('click', 'button', function (e) {
        $('.btnAdded').filter('[data-tag-id=' + e.currentTarget.dataset.tagId + ']').remove();
        console.log(addedItems.findIndex(item => item.tagId == e.currentTarget.dataset.tagId))
        addedItems.splice(addedItems.findIndex(item => item.tagId == e.currentTarget.dataset.tagId), 1)
        if (addedItems.length == 0) {
            $('#selectedGroupAll-' + addedPmId).hide()
        }
    })



    $('.startTagging').on('click', function (e) {
        if ($(this).html() == "Add Tags") {
            if ($('.startAnnotating').html() == "Save") {
                $('.startAnnotating').html("Add Annotations")
                $(this).html("Save")
                $('.startTagging').not('#startTagging-' + e.currentTarget.dataset.pmid).html("Edit Tags")
                $('.tagging').not('#tagging-' + e.currentTarget.dataset.pmid).slideUp()
                $('.selectedGroupAll').not('#selectedGroupAll-' + e.currentTarget.dataset.pmid).empty();
                $('.tagInput').not('#tagInput-' + e.currentTarget.dataset.pmid).val("");
                $('.results').not('#results-' + e.currentTarget.dataset.pmid).empty();
                $('#tagging-' + e.currentTarget.dataset.pmid).slideToggle();
                addedPmId = e.currentTarget.dataset.pmid;
                addedItems = [];
                $('#selectedGroup-' + addedPmId).empty();
            }
            $(this).html("Save")
            $('.startTagging').not('#startTagging-' + e.currentTarget.dataset.pmid).html("Edit Tags")
            $('.tagging').not('#tagging-' + e.currentTarget.dataset.pmid).slideUp()
            $('.selectedGroupAll').not('#selectedGroupAll-' + e.currentTarget.dataset.pmid).empty();
            $('.tagInput').not('#tagInput-' + e.currentTarget.dataset.pmid).val("");
            $('.results').not('#results-' + e.currentTarget.dataset.pmid).empty();
            $('#tagging-' + e.currentTarget.dataset.pmid).slideToggle();
            addedPmId = e.currentTarget.dataset.pmid;
            addedItems = [];
        } else if ($(this).html() == "Save") {
            let endpoint = e.currentTarget.dataset.url;
            console.log({ pmid: addedPmId, tags: addedItems })
            buttonNow = $(this)
            $.ajax({
                headers: { "X-CSRFToken": getCookie("csrftoken") },
                url: endpoint,
                type: "POST",
                //contentType: "application/json",
                data: JSON.stringify({ pmid: addedPmId, tags: addedItems }),
                dataType: 'json',
                success: function (response) {
                    if (response.status === "Success") {
                        location.reload();
                    } else if (response.status === "Failed") {

                        console.log(endpoint)
                        console.log(response)
                        console.log("failed")
                    }
                }

            })

        }
    });

    $('.startAnnotating').on('click', function (e) {
        if ($(this).html() == "Add Annotations") {
            if ($('.startTagging').html() == "Save") {
                $('.startTagging').html("Add Tags")
                $(this).html("Save")
                $('.startAnnotating').not('#startAnnotating-' + e.currentTarget.dataset.pmid).html("Edit Tags")
                $('.tagging').not('#tagging-' + e.currentTarget.dataset.pmid).slideUp()
                $('.selectedGroupAll').not('#selectedGroupAll-' + e.currentTarget.dataset.pmid).empty();
                $('.tagInput').not('#tagInput-' + e.currentTarget.dataset.pmid).val("");
                $('.results').not('#results-' + e.currentTarget.dataset.pmid).empty();
                $('#tagging-' + e.currentTarget.dataset.pmid).slideToggle();
                addedPmId = e.currentTarget.dataset.pmid;
                addedItems = [];
                $('#selectedGroup-' + addedPmId).empty();
            }
            $(this).html("Save")
            $('.startAnnotating').not('#startAnnotating-' + e.currentTarget.dataset.pmid).html("Edit Tags")
            $('.tagging').not('#tagging-' + e.currentTarget.dataset.pmid).slideUp()
            $('.selectedGroupAll').not('#selectedGroupAll-' + e.currentTarget.dataset.pmid).empty();
            $('.tagInput').not('#tagInput-' + e.currentTarget.dataset.pmid).val("");
            $('.results').not('#results-' + e.currentTarget.dataset.pmid).empty();
            $('#tagging-' + e.currentTarget.dataset.pmid).slideToggle();
            addedPmId = e.currentTarget.dataset.pmid;
            addedItems = [];
        } else if ($(this).html() == "Save") {
            let endpoint = e.currentTarget.dataset.url;
            console.log({ pmid: addedPmId, sub_text: selectedText, tags: addedItems })
            buttonNow = $(this)
            $.ajax({
                headers: { "X-CSRFToken": getCookie("csrftoken") },
                url: endpoint,
                type: "POST",
                //contentType: "application/json",
                data: JSON.stringify({ pmid: addedPmId, sub_text: selectedText, tags: addedItems }),
                dataType: 'json',
                success: function (response) {
                    if (response.status === "Success") {
                        location.reload();
                    } else if (response.status === "Failed") {

                        console.log(endpoint)
                        console.log(response)
                        console.log("failed")
                    }
                }

            })

        }
    });


    $('#tagDetail').on('click', function (e) {
        if (e.target.id == 'tagDetail') {
            $(this).css('display', 'none');
        }
    });


    $('#tagDetailCancel').on('click', function (e) {
        $('#tagDetail').css('display', 'none');
    });

    $('#tagDetailAddAllias').on('click', function (e) {
        //$('.alliasDiv').css('display','flex');
        $('.alliasDiv').show(500)
        $('.alliasDiv').css('display', 'flex');
        $(this).hide(500)
    });

    $('#tagDetailTag').on('click', function (e) {

        $('#selectedGroup-' + addedPmId).append('<button type="button" class="btn btnAdded btn-outline-success selected ml-2 mt-2" data-tag-id="' + e.currentTarget.dataset.tagId + '" data-tag="' + e.currentTarget.dataset.tag + '">' + e.currentTarget.dataset.tag + '</i></button>')
        addedItems.push({ "tag": e.currentTarget.dataset.tag, "tagId": e.currentTarget.dataset.tagId, "tagDescription": e.currentTarget.dataset.tagDescription, "tagAlias": $('#modalTagAlias').val() })

        $('#tagDetail').css('display', 'none');
    });

    $('#annotateDetail').on('click', function (e) {
        if (e.target.id == 'annotateDetail') {
            $(this).css('display', 'none');
        }
    });


    $('#annotateDetailCancel').on('click', function (e) {
        $('#annotateDetail').css('display', 'none');
    });

    $('#annotateDetailAddAllias').on('click', function (e) {
        //$('.alliasDiv').css('display','flex');
        $('.alliasDiv').show(500)
        $('.alliasDiv').css('display', 'flex');
        $(this).hide(500)
    });

    $('#annotateDetailTag').on('click', function (e) {
        $('#selectedGroup-' + addedPmId).append('<button type="button" class="btn btnAdded btn-outline-success selected ml-2 mt-2" data-tag-id="' + e.currentTarget.dataset.tagId + '" data-tag="' + e.currentTarget.dataset.tag + '">' + e.currentTarget.dataset.tag + '</i></button>')
        addedItems.push({ "tag": e.currentTarget.dataset.tag, "tagId": e.currentTarget.dataset.tagId, "tagDescription": e.currentTarget.dataset.tagDescription, "tagAlias": $('#modalTagAlias').val() })

        $('#annotateDetail').css('display', 'none');
    });


});
