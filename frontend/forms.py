from django import forms
from datetime import datetime, date

class searchForm(forms.Form):
    keyword = forms.CharField(
        min_length=3, max_length=60, required=True, label='')
    # Widgets
    keyword.widget.attrs.update(
        {'class': 'form-control tagmed-input ', 'aria-describedby': 'searchHelp', 'placeholder': 'Search on MediPub', 'name': 'keyword'})


class reportArticleForm(forms.Form):
    # Fields
    report_message = forms.CharField(widget=forms.Textarea,
                                     label="Article Report Form", min_length=1, max_length=300, required=True)

    # Widgets
    report_message.widget.attrs.update(
        {'class': 'form-control', 'placeholder': 'Please enter your report message to be processed'})


class advancedSearchForm(forms.Form):
    
    ARTICLE_PARTS = (('all', 'all'),
                     ('title', 'title'),
                     ('abtract','abstract'),
                     ('authors','authors'),
                     ('tags', 'tags'),
                     ('highlights', 'highlights'))
    
    start_date = forms.DateField(initial=date.today())
    end_date = forms.DateField(initial=date.today())
    is_reviewed = forms.BooleanField(initial= True, required=True)

    article_parts = forms.ChoiceField(
        widget=forms.Select,
        choices=ARTICLE_PARTS,
    )

    keyword = forms.CharField(
        min_length=3, max_length=50, required=True, label='')

    # Widgets
    keyword.widget.attrs.update(
        {'class': 'form-control', 'placeholder': 'Enter the advanced search term', 'name': 'keyword'})
  


class customTagForm(forms.Form):
    # Fields
    custom_tag = forms.CharField(min_length=3, max_length=200, required=True, label='')

    # Widgets
    custom_tag.widget.attrs.update(
        {'class': 'form-control', 'placeholder': 'Please enter your custom tag'})

class customAnnotationForm(forms.Form):
    # Fields
    custom_annotation = forms.CharField(min_length=3, max_length=50, required=True, label='')

    # Widgets
    custom_annotation.widget.attrs.update(
        {'class': 'form-control', 'placeholder': 'Please enter your custom annotation tag'})

class tagSeach(forms.Form):
    # Fields
    query_term = forms.CharField(
        label="Wikidata Query Term", min_length=1, max_length=40, required=True)

    # Widgets
    query_term.widget.attrs.update(
        {'class': 'form-control', 'placeholder': 'Enter the query term'})



class userSearchForm(forms.Form):
    # Fields
    user_search_field = forms.CharField(min_length=3, max_length=20, required=True, label='')

    # Widgets
    user_search_field.widget.attrs.update(
        {'class': 'form-control', 'placeholder': 'Please enter username to search'})


class tagSearchForm(forms.Form):
    # Fields
    tag_search_field = forms.CharField(min_length=3, max_length=20, required=True, label='')

    # Widgets
    tag_search_field.widget.attrs.update(
        {'class': 'form-control', 'placeholder': 'Please enter tag to search'})
