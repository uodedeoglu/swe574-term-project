from django.urls import path
from . import views


urlpatterns = [
     path('', views.index, name='index'),
     path('sort/<int:sort_type>/results/', views.basic_search_result, name='results'),
     path('details/<int:pmid>', views.show_article_detail, name='details'),

    # Report
     path('report/<int:pmid>',
         views.show_report_page, name="show_report"),
     path('report_submit/', views.submit_report_page, name="submit_report"),

    # Highlight
    path('annotate/<int:pmid>/<str:sub_text>/', views.show_annotate_page, name="show_annotate"),
    path('annotate/<int:pmid>/', views.show_annotate_page, name="show_annotate"),
    path('annotate_submit/', views.submit_annotate_page,  name="submit_annotate"),
    path('annotations/', views.annotate_article, name="annotate_article"),

     # Advanced Search
     path('advanced', views.navigate_advanced_search, name='advanced'),
     path('sort/<int:sort_type>/advanced-search', views.advanced_search, name='advancedSearch'),
     
     # Query Save
     path('save_query', views.save_query, name="save_query"),
     path('delete_query', views.delete_query, name="delete_query"),

     # Custom Tag
     path('custom/<int:pmid>', views.show_custom_tag_page, name="show_custom_tag"),
     path('custom_tag/', views.submit_custom_tag, name='submit_custom'),

     # Tag
     path('searchtag/', views.search_tag, name="search_tag"),
     path('tags/', views.tag_article, name="tag_article"),

     # Profile
     path('profile/', views.show_profile, name="profile"),

     # Follow Article
     path('follow/<int:pmid>', views.follow_article, name="follow_article"),

     # Unfollow Article
     path('unfollow/<int:pmid>', views.unfollow_article, name="unfollow_article"),

     # Follow User
     path('follow_user/<str:email>', views.follow_user, name="follow_user"),

     # Unfollow User
     path('unfollow_user/<str:email>', views.unfollow_user, name="unfollow_user"),

     # Follow Tag
     path('follow_tag/<str:unique_id>', views.follow_tag, name="follow_tag"),

     # Unfollow Tag
     path('unfollow_tag/<str:unique_id>', views.unfollow_tag, name="unfollow_tag"),

     # Feed
     path('feed/', views.show_feed, name="feed"),

     # User Lookup Page
     path('show_user_lookup/', views.show_user_lookup, name="user_lookup"),

      # User Search
     path('search_user_lookup/', views.search_user_lookup, name="search_user"),

     # Tag Lookup Page
     path('show_tag_lookup/', views.show_tag_lookup, name="tag_lookup"),

     # Tag Search
     path('search_tag_lookup/', views.search_tag_lookup, name="search_tag"),

     # Condition of Use
     path('conditions/', views.show_condition_of_use, name="conditions"),
     # Privacy Notice
     path('privacy/', views.show_privacy_notice, name="privacy"),

          # Signup Request
     path('signup_request/', views.show_signup_request, name="signup_requests"),

     # Approve User Request
     path('approve_request/<str:email>', views.approve_user_request, name="approve_request"),

     # Reject User Request
     path('reject_request/<str:email>', views.reject_user_request, name="reject_request"),
]
